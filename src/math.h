#ifndef MATH_H_
#define MATH_H_


/*
libc/ieee_float/ieee_float.h
Created:    Oct 14, 1993 by Philip Homburg <philip@cs.vu.nl>
Define structures and macros for manipulating IEEE floats
*/

#ifndef IEEE_FLOAT_H
#define IEEE_FLOAT_H

#define isnan __IsNan

struct f64
{
    unsigned int low_word;
    unsigned int high_word;
};

#define F64_SIGN_SHIFT    31
#define F64_SIGN_MASK    1

#define F64_EXP_SHIFT    20
#define F64_EXP_MASK    0x7ff
#define F64_EXP_BIAS    1023
#define F64_EXP_MAX    2047

#define F64_MANT_SHIFT    0
#define F64_MANT_MASK    0xfffff

#define F64_GET_SIGN(fp)    (((fp)->high_word >> F64_SIGN_SHIFT) & \
                    F64_SIGN_MASK)
#define F64_GET_EXP(fp)        (((fp)->high_word >> F64_EXP_SHIFT) & \
                    F64_EXP_MASK)
#define F64_SET_EXP(fp, val)    ((fp)->high_word= ((fp)->high_word &    \
                ~(F64_EXP_MASK << F64_EXP_SHIFT)) |     \
                (((val) & F64_EXP_MASK) << F64_EXP_SHIFT))

#define F64_GET_MANT_LOW(fp)        ((fp)->low_word)
#define F64_SET_MANT_LOW(fp, val)    ((fp)->low_word= (val))
#define F64_GET_MANT_HIGH(fp)    (((fp)->high_word >> F64_MANT_SHIFT) & \
                    F64_MANT_MASK)
#define F64_SET_MANT_HIGH(fp, val)    ((fp)->high_word= ((fp)->high_word & \
                ~(F64_MANT_MASK << F64_MANT_SHIFT)) |    \
                (((val) & F64_MANT_MASK) << F64_MANT_SHIFT))

#endif /* IEEE_FLOAT_H */

/*
 * $PchId: ieee_float.h,v 1.3 1996/02/22 21:01:39 philip Exp $
 */

template <typename T>
T clamp(T min, T x, T max)
{
    if (x < min) return min;
    if (x > max) return max;
    return x;
}

template <typename T>
T min(T a, T b)
{
    if (a < b)  return a;
    return b;
}

template <typename T>
T min(T a, T b, T c)
{
    return min(min(a, b), c);
}

template <typename T>
T max(T a, T b)
{
    if (a > b)  return a;
    return b;
}

template <typename T>
T max(T a, T b, T c)
{
    return max(max(a, b), c);
}

template <typename T>
T mod(T a, T b)
{
    return a % b;
}

template <typename T>
T abs(T a)
{
    return a < 0 ? a*-1 : a;
}

int my_floor(float x)
{
    auto i = int(x);
    if (i > x) i--;
    return i;
}

int my_ceil(float x)
{
    auto i = int(x);
    if (i < x) i++;
    return i;
}

int my_trunc(float d) {
    return (d>0) ? (my_floor(d)) : (my_ceil(d));
}

int my_fmod(float x, float y)
{
    return x - my_trunc(x / y) * y;
}

int my_pow(int x, unsigned int y)
{
    if (y == 0)            return 1;
    else if ((y%2) == 0)   return my_pow(x, y/2) * my_pow(x, y/2);
    else                   return x * my_pow(x, y/2) * my_pow(x, y/2);
}

// Not even sure what this does ngl.
// Source: https://github.com/jncraton/minix3/blob/master/lib/gnu/ieee_float/ldexp.c
double my_ldexp(double value, int exp)
{
    struct f64 *f64p;
    int oldexp;
    double factor;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
    f64p= (struct f64 *)&value;
#pragma GCC diagnostic pop
    // int exp_bias= 0;

    oldexp= F64_GET_EXP(f64p);
    if (oldexp == F64_EXP_MAX)
    {    /* Either infinity or Nan */
        return value;
    }
    if (oldexp == 0)
    {
        /* Either 0 or denormal */
        if (F64_GET_MANT_LOW(f64p) == 0 &&
            F64_GET_MANT_HIGH(f64p) == 0)
        {
            return value;
        }
    }

    /* If exp is too large (> 2*F64_EXP_MAX) or too small
     * (< -2*F64_EXP_MAX) return HUGE_VAL or 0. This prevents overflows
     * in exp if exp is really weird
     */
    if (exp >= 2*F64_EXP_MAX)
    {
        return 694206942069420;
    }
    if (exp <= -2*F64_EXP_MAX)
    {
        return 0;
    }

    /* Normalize a denormal */
    if (oldexp == 0)
    {
        /* Multiply by 2^64 */
        factor= 65536.0;    /* 2^16 */
        factor *= factor;    /* 2^32 */
        factor *= factor;    /* 2^64 */
        value *= factor;
        exp= -64;
        oldexp= F64_GET_EXP(f64p);
    }

    exp= oldexp + exp;
    if (exp >= F64_EXP_MAX)
    {    /* Overflow */
        return 694206942069420;
    }
    if (exp > 0)
    {
        /* Normal */
        F64_SET_EXP(f64p, exp);
        return value;
    }
    /* Denormal, or underflow. */
    exp += 64;
    F64_SET_EXP(f64p, exp);
    /* Divide by 2^64 */
    factor= 65536.0;    /* 2^16 */
    factor *= factor;    /* 2^32 */
    factor *= factor;    /* 2^64 */
    value /= factor;
    if (value <= 0.000000001)
    {
        /* Underflow */
    }
    return value;
}

#endif // MATH_H_
