#include <fcntl.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "./hotloader.h"

#include "./macros.h"
#include "./containers.h"

Hotloaded_File load(const char* filepath, Hotloaded_File_On_Load on_load = nullptr)
{
    Hotloaded_File hotloaded_file { };
    hotloaded_file.filepath = String_View(filepath, strlen(filepath));
    hotloaded_file.on_load = on_load;

    // @Robustness: make sure open didn't fail.
    hotloaded_file.fd = open(filepath, O_RDONLY);
    if (hotloaded_file.fd < 0) {
        printf("ERROR: Couldn't open file `%s`. Check it exists\n", filepath);
        ASSERT(0);
    }

    struct stat file_stat;
    if (fstat(hotloaded_file.fd, &file_stat) < 0) {
        printf("ERROR: Couldn't get stat for file `%s`. Check it exists\n", filepath);
        ASSERT(0);
    }

    printf("File size: %ld\tLast file modification: %s\n", file_stat.st_size, ctime(&file_stat.st_mtime));
    hotloaded_file.time_last_modified = file_stat.st_mtime;

    hotloaded_file.contents = (char*) mmap(0,
                                           file_stat.st_size,
                                           PROT_READ,
                                           MAP_PRIVATE,
                                           hotloaded_file.fd,
                                           0);
    hotloaded_file.contents_size = strlen(hotloaded_file.contents);

    if (hotloaded_file.on_load != nullptr)
        hotloaded_file.on_load(hotloaded_file.contents, hotloaded_file.contents_size);

    return hotloaded_file;
}

bool hotload(Hotloaded_File &file)
{
    struct stat file_stat;
    if (fstat(file.fd, &file_stat) < 0) {
        printf("ERROR: Couldn't get stat for file `%.*s`. Check it exists\n", (int)file.filepath.count, file.filepath.data);
        ASSERT(0);
    }

    // printf("File size: %ld\t%ld == %ld\n", file_stat.st_size, file_stat.st_mtime, file.time_last_modified);

    // @Todo @Speed: only reload the file data if the file has actually changed.
    if (file.time_last_modified >= (u64)file_stat.st_mtime)
        return false; // No need to do anything...

    file.time_last_modified = file_stat.st_mtime;

    // if (file.contents)  free(file.contents);

    auto file_contents = (char*) mmap(0, file_stat.st_size, PROT_READ, MAP_PRIVATE, file.fd, 0);
    file.contents = file_contents;
    file.contents_size = strlen(file_contents);

    if (file.on_load != nullptr)
        file.on_load(file.contents, file.contents_size);

    return true;
}
