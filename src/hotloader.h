#ifndef HOTLOADER_H_
#define HOTLOADER_H_

#include "containers.h"

using Hotloaded_File_On_Load = void (*) (char *, size_t);

struct Hotloaded_File
{
    Hotloaded_File_On_Load  on_load;

    String_View filepath { };
    int fd { -1 };
    char *contents { nullptr };
    size_t contents_size { 0 };

    bool did_load { false };

    uint64_t time_last_modified { 0 };
};


#endif // HOTLOADER_H_
