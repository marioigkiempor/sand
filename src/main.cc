// C++ includes. Should remove.
#include <iostream>

// C includes.
#include <stdio.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "SDL2/SDL_ttf.h"
#include "SDL2_gfxPrimitives.h"

#include "./dbg.h"
#include "./macros.h"
#include "./math.h"
#include "./containers.h"

#include "./renderer.h"

////////////////////////////////////////
///    Types
////////////////////////////////////////

#define hex(c) HEX_COLOR(((int)c.x << 24) | ((int)c.y << 16) |((int)c.z << 8) |((int)c.w << 0))

////////////////////////////////////////
///    Game
////////////////////////////////////////
struct App {
    SDL_Window* window { nullptr };
    SDL_Renderer* sdl_renderer { nullptr };
    SDL_GLContext gl_context;

    Renderer renderer;

    bool quit { false };

    float dt;

    struct {
        glm::ivec2 pos { };
        bool pressed { false };
        bool released { false };
    } mouse_state;

    struct {
        glm::vec4 tile_empty_color       = glm::vec4(0x565857FF);
        glm::vec4 tile_wall_color        = glm::vec4(0x8A8D91FF);
        glm::vec4 tile_player_color      = glm::vec4(0xF5D3C8FF);
        glm::vec4 tile_target_color      = glm::vec4(0xE98A15FF);
        glm::vec4 tile_objective_color   = glm::vec4(0X734B5EFF);
        glm::vec4 tile_door_open_color   = glm::vec4(0X378A41FF);
        glm::vec4 tile_door_closed_color = glm::vec4(0X732420FF);

        glm::vec4 bg_color { 0x27, 0x27, 0x27, 0xFF };
    } visuals;
};

enum class Direction
{
    NORTH = 0,
    SOUTH,
    EAST,
    WEST
};

using Grid_Coord = glm::ivec2;

struct Tile
{
    enum class Kind
    {
    EMPTY,
    WALL,
    SAND,
    WATER,
    };

    Kind kind;
    Grid_Coord coord;

    Renderable renderable;
};


///
///    Globals
///

const char*   APP_NAME       = "Snad";
int           WINDOW_WIDTH   = 1920/4;
int           WINDOW_HEIGHT  = 1080/4;
int           WINDOW_PADDING = 50;
glm::ivec2    tile_size       { 10, 10 };
glm::ivec2    grid_size       { WINDOW_WIDTH/tile_size.x, WINDOW_HEIGHT/tile_size.y };

App           app             { };

constexpr u32 RENDER_FLAGS  = SDL_RENDERER_ACCELERATED;
constexpr u32 WINDOW_FLAGS  = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

Array<Tile>   g_tiles         { };
Tile::Kind    placing_kind  = Tile::Kind::SAND;

float         gravity       = -2.0f;

////////////////////////////////////////
///    SDL layer
////////////////////////////////////////
#define lib_error(lib, message, ...)                        \
    do {                                                    \
        fprintf(stderr, "[" lib " ERROR]: ");               \
        fprintf(stderr, "%s\n\t", message, ##__VA_ARGS__);  \
        fprintf(stderr, "\n");                              \
        fflush(stderr);                                     \
        exit(1);                                         \
    } while(0);

#define SDL_ERROR(message, ...) lib_error("SDL", message, ##__VA_ARGS__)
#define TTF_ERROR(message, ...) lib_error("TTF", message, ##__VA_ARGS__)

int sc(int code)
{
    if (code < 0) {
        SDL_ERROR(SDL_GetError());
    }

    return code;
}

void* sc(void* code)
{
    if (!code) {
        SDL_ERROR(SDL_GetError());
    }

    return code;
}

int tc(int code)
{
    if (code < 0) {
        TTF_ERROR(TTF_GetError());
    }

    return code;
}

void* tc(void* code)
{
    if (!code) {
        TTF_ERROR(TTF_GetError());
    }

    return code;
}

void init_renderer(void)
{
    ///
    /// Init SDL
    ///

    sc(SDL_Init(SDL_INIT_EVERYTHING));
    printf("[INFO] Initialised SDL Version %d.%d\n", SDL_MAJOR_VERSION, SDL_MINOR_VERSION);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    app.window   = (SDL_Window*) sc(SDL_CreateWindow(APP_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FLAGS));
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

    app.gl_context = (SDL_GLContext) sc(SDL_GL_CreateContext(app.window));
    SDL_GL_SetSwapInterval(1);

    app.sdl_renderer = (SDL_Renderer*) sc(SDL_CreateRenderer(app.window, -1, RENDER_FLAGS));

    if (!init_gl(app.renderer, WINDOW_WIDTH, WINDOW_HEIGHT)) {
        printf("There were errors initializing the OpenGL context. Hopefully there's some explanation why above ^.\n");
    }

    ///
    /// Init font rendering
    ///

    tc(TTF_Init());
    printf("[INFO] Initialised TTF Version %d.%d\n", TTF_MAJOR_VERSION, TTF_MINOR_VERSION);

    // debug_font = (TTF_Font*) tc(TTF_OpenFont("./src/debug.ttf", 24));
}

Tile* get_tile(int x, int y)
{
    if (x < 0)  x = 0;
    if (y < 0)  y = 0;

    return &g_tiles[y * grid_size.x + x];
}

bool tile_empty(int x, int y)
{
    if (x >= grid_size.x || y >= grid_size.y)  return false;

    Tile *target = get_tile(x, y);
    if (!target) return true;

    if (target->kind == Tile::Kind::EMPTY)  return true;

    return false;
}

void move(Tile* tile, int x, int y)
{
    float dx = ((float)x - (float)tile->coord.x);
    float dy = ((float)y - (float)tile->coord.y);

    float step = abs(dy);
    if (abs(dx) >= abs(dy))  step = abs(dx);

    dx = dx/step;
    dy = dy/step;

    float tx = (float)tile->coord.x;
    float ty = (float)tile->coord.y;

    auto i=1;
    while (i <= step) {
        if (!tile_empty(floor(tx), floor(ty)))  break;

        tx = tx + dx;
        ty = ty + dy;
        i += 1;
    }

    // Tile *target = get_tile(floor(tx), floor(ty));
    Tile *target = get_tile(x, y);
    if (!target)  return;

    auto my_kind = tile->kind;
    tile->kind = Tile::Kind::EMPTY;
    target->kind = my_kind;
}

Tile* tile_at_mouse_coord(int mouse_x, int mouse_y)
{
    auto tx = (int)((mouse_x / (float)WINDOW_WIDTH)  * (float)grid_size.x);
    auto ty = (int)((mouse_y / (float)WINDOW_HEIGHT) * (float)grid_size.y);

    return get_tile(tx, ty);
}

void handle_input()
{
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
        if (SDL_QUIT == e.type) {
            app.quit = true;
            break;
        }
        else if (SDL_KEYDOWN == e.type) {
            if (SDL_SCANCODE_Q == e.key.keysym.scancode) {
                app.quit = true;
            }
            else if (SDL_SCANCODE_1 == e.key.keysym.scancode) {
                placing_kind = Tile::Kind::SAND;
            }
            else if (SDL_SCANCODE_2 == e.key.keysym.scancode) {
                placing_kind = Tile::Kind::WATER;
            }
        }
        else if (e.type == SDL_MOUSEMOTION ||
                 e.type == SDL_MOUSEBUTTONDOWN ||
                 e.type == SDL_MOUSEBUTTONUP) {
            SDL_GetMouseState(&app.mouse_state.pos.x, &app.mouse_state.pos.y);

            if (e.type == SDL_MOUSEBUTTONDOWN) {
                app.mouse_state.pressed = true;
            }
            if (e.type == SDL_MOUSEBUTTONUP) {
                app.mouse_state.pressed = false;
            }
            if (e.type == SDL_MOUSEMOTION) {
                if (app.mouse_state.pressed) {
                    auto *tile = tile_at_mouse_coord(app.mouse_state.pos.x, app.mouse_state.pos.y);
                    if (tile)  tile->kind = placing_kind;
                }
            }
        }
    }
}

int main()
{
    init_renderer();
    defer(SDL_Quit());


    Renderable renderable { };
    init(renderable, app.renderer.quad_shader, app.renderer.quad_vertices, app.renderer.quad_indices);

    bind(renderable.vao);
    GLuint instance_pos_buffer;
    glGenBuffers(1, &instance_pos_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, instance_pos_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::ivec2)*grid_size.x*grid_size.y, NULL, GL_STREAM_DRAW);
    glVertexAttribDivisor(2, grid_size.x*grid_size.y);

    { // Init game
        // printf("win size is: %d %d, tile res is: %d %d\n", WINDOW_WIDTH, WINDOW_HEIGHT, tile_res.x, tile_res.y);
        for (int y = 0; y < grid_size.y; ++y) {
            for (int x = 0; x < grid_size.x; ++x) {
                Tile new_tile = {
                    .kind       = Tile::Kind::EMPTY, //(x%2 == 0) ? Tile::Kind::EMPTY : Tile::Kind::WALL,
                    .coord      = glm::ivec2(x, y),
                    .renderable = renderable
                };

                g_tiles.push(new_tile);
            }
        }
    }

    get_tile(grid_size.x/2, grid_size.y-1)->kind = Tile::Kind::SAND;
    get_tile(grid_size.x/2, grid_size.y-1)->kind = Tile::Kind::SAND;
    get_tile(grid_size.x/2, grid_size.y-2)->kind = Tile::Kind::SAND;
    get_tile(grid_size.x/2, grid_size.y-3)->kind = Tile::Kind::SAND;
    get_tile(grid_size.x/2, grid_size.y-4)->kind = Tile::Kind::SAND;
    get_tile(grid_size.x/2, grid_size.y-5)->kind = Tile::Kind::SAND;

    app.dt = 0.0f;


    uint32_t fps = 30;
    auto fps_interval = 1000.0f / (float)fps;
    UNUSED(fps_interval);

    while (!app.quit) {
        uint64_t start_of_frame_counter = SDL_GetPerformanceCounter();

        handle_input();

        // Update states.
        // update(game.current_level.player);
        {
            for (size_t y = grid_size.y-1; (int)y >= 0; --y) {
                for (size_t x = 0; (int)x < grid_size.x; ++x) {
                    Tile *curr_tile = get_tile(x, y);
                    if (!curr_tile)  continue;

                    switch(curr_tile->kind) {
                    case Tile::Kind::EMPTY: break;

                    case Tile::Kind::WALL: {
                    } break;

                    case Tile::Kind::WATER: {
                        if (tile_empty(x, y+1)) {
                            move(curr_tile, x, y+1);
                        }
                        else if (tile_empty(x-1, y+1)) {
                            move(curr_tile, x-1, y+1);
                        }
                        else if (tile_empty(x+1, y+1)) {
                            move(curr_tile, x+1, y+1);
                        }
                        else {
                            float float_left_chance = (float)rand()/(float)RAND_MAX;
                            if (float_left_chance < 0.5f) {
                                if (tile_empty(x-1, y)) {
                                    move(curr_tile, x-1, y);
                                }
                                else if (tile_empty(x+1, y)) {
                                    move(curr_tile, x+1, y);
                                }
                            }
                            else {
                                if (tile_empty(x+1, y)) {
                                    move(curr_tile, x+1, y);
                                }
                                else if (tile_empty(x-1, y)) {
                                    move(curr_tile, x-1, y);
                                }
                            }
                        }
                    } break;

                    case Tile::Kind::SAND: {
                        if (tile_empty(x, y+1)) {
                            move(curr_tile, x, y+1);
                        }
                        else if (tile_empty(x-1, y+1)) {
                            move(curr_tile, x-1, y+1);
                        }
                        else if (tile_empty(x+1, y+1)) {
                            move(curr_tile, x+1, y+1);
                        }
                    } break;

                    default: UNREACHABLE();
                    }
                }
            }
        }

        { // Render

            // @Todo: renderer_begin_scene
            clear_screen({0.2, 0.2, 0.3});

            begin_scene(app.renderer, app.renderer.camera);

            glm::ivec2 instanced_grid_positions[grid_size.x*grid_size.y] = {};
            size_t i =0;
            for(int y=0; y < grid_size.y; ++y) {
                for(int x=0; x < grid_size.x; ++x) {
                    instanced_grid_positions[i++] = glm::ivec2(x,y);
                }
            }

            // for(int y=0; y < grid_size.y; ++y) {
            //     for(int x=0; x < grid_size.x; ++x) {
            //         printf("(%d,%d) ", instanced_grid_positions[y * grid_size.x + x].x, instanced_grid_positions[y * grid_size.x + x].y);
            //     }
            //     printf("\n");
            // }
            bind(renderable.vao);
            use_shader(app.renderer.quad_shader);

            glBindBuffer(GL_ARRAY_BUFFER, instance_pos_buffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::ivec2)*grid_size.x*grid_size.y, instanced_grid_positions, GL_STREAM_DRAW);
            glVertexAttribIPointer(2, 2, GL_INT, 0, (void*)0);
            glVertexAttribDivisor(glGetUniformLocation(renderable.shader.id, "instanced_pos"), 1);
            // glVertexAttribDivisor(2, grid_size.x*grid_size.y);

            glm::vec3 tile_col = glm::vec3(1.0f, 1.0f, 1.0f);
            set_uniform(renderable.shader, "u_tile_col", tile_col);

            set_uniform(app.renderer.quad_shader, "u_view_projection", app.renderer.state.view_projection_matrix);

            bind(renderable.vao);
            bind(renderable.ibo);
            glDrawElementsInstanced(GL_TRIANGLE_STRIP,
                                    renderable.vao.index_buffer->data.count,
                                    GL_UNSIGNED_INT,
                                    0,
                                    grid_size.x*grid_size.y);


            for (auto &tile : g_tiles) {
                use_shader(tile.renderable.shader);
                set_uniform(tile.renderable.shader, "u_grid_pos", tile.coord);
                set_uniform(tile.renderable.shader, "u_tile_size", tile_size);

                glm::vec3 col = glm::vec3(1.0f, 1.0f, 1.0f);
                switch (tile.kind) {
                    case Tile::Kind::WALL:   col = glm::vec3(0.3f, 0.4f, 0.2f); break;
                    case Tile::Kind::WATER:  col = glm::vec3(0.1f, 0.4f, 0.7f); break;
                    case Tile::Kind::EMPTY:  col = glm::vec3(0.1f, 0.2f, 0.2f); break;
                    case Tile::Kind::SAND:   col = glm::vec3(0.6f, 0.4f, 0.1f); break;
                    default: UNREACHABLE(); break;
                }

                set_uniform(tile.renderable.shader, "u_tile_col", col);

                // renderer_submit(app.renderer, tile.renderable.shader, tile.renderable.vao);
            }

            // for (auto &renderable : app.renderer.renderables) {
            //     renderer_submit(app.renderer, renderable.shader, renderable.vao);
            // }

            end_scene(app.renderer);

            SDL_GL_SwapWindow(app.window);

            SDL_RenderPresent(app.sdl_renderer);
        }

        // Update dt.

        uint64_t end_of_frame_counter = SDL_GetPerformanceCounter();
        float elapsed_ms = (end_of_frame_counter-start_of_frame_counter) / (float) SDL_GetPerformanceFrequency()*1000.f;
        UNUSED(end_of_frame_counter);
        UNUSED(elapsed_ms);

        // SDL_Delay(floorf(fps_interval - elapsed_ms));
    } // end of game loop

    ///
    /// Shutdown, cleanup, all that shit
    ///

    // save_level(game.current_level);

    ///
    /// Finished
    ///

    printf("Application finished.\n");
    return 0;
}
